<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Hashing\HashManager;

class RegisterController extends Controller
{
    use RegisterableTrait;
    
    /**
     * @param Request $request
     * @param HashManager $hashManager
     */
    public function register(Request $request, HashManager $hashManager)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'account_type' => 'required|in:recruiter,jobseeker'
        ]);
        
        $registerable = $this->getRegisterableEntity($request->account_type);
        $registerable->store($request->email, $hashManager->make($request->password));

        return redirect('/');
    }
}
