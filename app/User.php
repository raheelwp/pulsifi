<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use App\Registerable;

class User extends Authenticatable implements Registerable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @param string $email
     * @param string $password
     */
    public function store(string $email, string $password)
    {
        $this->email = $email;
        $this->password = $password;
        $this->save();
    }

    /**
     * @param Builder $query
     * @param string $email
     */
    public function scopeEmail(Builder $query, string $email): Builder
    {
        return $query->where('email', $email);
    }

    /**
     * @param Builder $query
     * @param string $password
     */
    public function scopePassword(Builder $query, string $password): Builder
    {
        return $query->where('password', $password);
    }

    /**
     * @return null
     */
    public function getRememberTokenName()
    {
        return null;
    }
}
