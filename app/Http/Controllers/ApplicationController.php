<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Job;
use DateTime;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var AuthManager
     */
    private $authManager;
    
    /**
     * @param Request $request
     * @param AuthManager $authManager
     */
    public function __construct(Request $request, AuthManager $authManager)
    {
        $this->request = $request;
        $this->authManager = $authManager;
    }
    
    /**
     * @param Application $application
     * @param Job $job
     * @return View
     */
    public function index(Application $application, Job $job): View
    {
        $applications = DB::table('applications')
            ->join('jobs', 'jobs.id', '=', 'applications.job_id')
            ->join('jobseekers', 'jobseekers.id', '=', 'applications.jobseeker_id')
            ->select('jobs.title', 'jobseekers.email', 'applications.created_at')
            ->where('jobs.recruiter_id', $this->authManager->guard('recruiter')->id())
            ->get();
        
        return view('recruiter/applications', ['applications' => $applications]);   
    }

    /**
     * @param Application $application
     * @param Job
     */
    public function store (
        Application $application,
        int $jobId
    ): View {
        $now = (new DateTime())->format('Y-m-d H:i:s');
        $application->job_id = $jobId;
        $application->jobseeker_id = $this->authManager->guard('jobseeker')->id();
        $application->created_at = $now;
        $application->created_at = $now;
        $application->save();

        return view('jobseeker/success');
    }
}
