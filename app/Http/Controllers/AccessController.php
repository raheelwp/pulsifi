<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Hashing\HashManager;
use Illuminate\Auth\AuthManager;

class AccessController extends Controller
{
    use RegisterableTrait;

    /**
     * @param Request $request
     * @param HashManager $hashManager
     * @param AuthManager $authManager
     */
    public function login (
        Request $request, 
        HashManager $hashManager,
        AuthManager $authManager
    ) {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'account_type' => 'required|in:recruiter,jobseeker'
        ]);

        $registerable = $this->getRegisterableEntity($request->account_type);
        $authenticableUser = $registerable->email($request->email)->first();
        if ($authenticableUser === null || 
            $hashManager->check($request->password, $authenticableUser->password) === false) {
            return back()->withErrors([
                'failed_login' => 'email or password incorrect'
            ]);
        }
        
        $authManager->guard($request->account_type)->login($authenticableUser);
        return redirect()->intended($authManager->guard($request->account_type)->user()->redirectTo);
    }

    /**
     * @param AuthManager $authManager
     */
    public function logout(AuthManager $authManager, string $accountType)
    {
        $authManager->guard($accountType)->logout();
        return redirect('/');
    }
}
