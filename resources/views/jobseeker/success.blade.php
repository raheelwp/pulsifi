<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pulsifi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ url('jobseeker/dashboard') }}">Pulsifi</a>
            <span class="navbar-text">
                 <a class="nav-link" href="{{ url('jobseeker/logout') }}">Logout</a>
            </span>
        </nav>
    </div>
    <div class="container">
        <div class="col success">
            <span class="alert alert-success">Congratulatios, your application has been submitted.</span>
        </div>
    </div>
</body>
</html>
