<?php

namespace App;

use App\User;

class Recruiter extends User 
{    
    /**
     * @var string
     */
    public $redirectTo = 'recruiter/dashboard';

    /**
     * @var string
     */
    protected $table = 'recruiters';
}
