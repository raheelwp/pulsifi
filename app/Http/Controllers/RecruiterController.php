<?php

namespace App\Http\Controllers;

use App\Recruiter;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthManager;
use Illuminate\View\View;
use App\Job;

class RecruiterController extends Controller
{
    /**
     * @param Request $request
     * @param AuthManager $authManager
     * @param Job $job
     * @return View
     */
    public function index(Request $request, AuthManager $authManager, Job $job): View
    {
        $jobs = $job->recruiter($authManager->guard('recruiter')->id())->get();
        return view('recruiter/dashboard', ['jobs' => $jobs]);   
    }
}
