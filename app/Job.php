<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Job extends Model
{
    /**
     * @var string
     */
    protected $table = 'jobs';

    /**
     * @param Builder $query
     * @param int $id
     */
    public function scopeRecruiter(Builder $query, int $id): Builder
    {
        return $query->where('recruiter_id', $id);
    }
}
