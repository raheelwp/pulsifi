<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pulsifi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ url('recruiter/dashboard') }}">Pulsifi</a>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('recruiter/job/post') }}">Post Job</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('recruiter/applications') }}">View Applications</a>
                    </li>
                </ul>
                <span class="navbar-text">
                    <a class="nav-link" href="{{ url('recruiter/logout') }}">Logout</a>
                </span>
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="col">
            @foreach($jobs as $job)
                <div class="card job-list">
                    <div class="card-header">
                        {{ $job->title }}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $job->location }}</h5>
                        <blockquote class="blockquote">
                            <p class="mb-0">{{ $job->description }}</p>
                            <footer class="blockquote-footer">{{ $job->created_at->toFormattedDateString() }}</footer>
                        </blockquote>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</body>
</html>
