<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use Illuminate\Auth\AuthManager;

class JobController extends Controller
{
    /**
     * @return View
     */
    public function create(): View
    {
        return view('recruiter/post_job');
    }

    /**
     * @param Request $request
     * @param Job $job
     * @return View
     */
    public function index(Request $request, Job $job): View
    {
        $jobs = $job->all();
        return view('jobseeker/dashboard', ['jobs' => $jobs]);   
    }

    /**
     * @param Request $request
     * @param AuthManager $authManager
     * @param Job $job
     * @return RedirectResponse
     */
    public function store(Request $request, AuthManager $authManager, Job $job): RedirectResponse
    {
        $request->validate([
            'title' => 'required|max:50',
            'location' => 'required|max:50',
            'description' => 'required',
            'date' => 'required|date'
        ]);

        $job->title = $request->title;
        $job->location = $request->location;
        $job->description = $request->description;
        $job->recruiter_id = $authManager->guard('recruiter')->id();
        $job->created_at = $request->date;
        $job->updated_at = $request->date;
        $job->save();

        return redirect('recruiter/dashboard');
    }

    
}
