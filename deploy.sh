#!/usr/bin/env bash

cd /home/ubuntu/pulsifi
sudo git stash
sudo git pull origin master
sudo docker-compose -f docker/production.yml down
sudo cp ./.env.production ./.env
sudo docker-compose -f docker/production.yml up --build -d
sudo docker exec php bash -c "chmod -R 777 storage && composer install"

exit 0


