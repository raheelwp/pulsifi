<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pulsifi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ url('recruiter/dashboard') }}">Pulsifi</a>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('recruiter/job/post') }}">Post Job</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ ('recruiter/applications') }}">View Applications</a>
                    </li>
                </ul>
                <span class="navbar-text">
                    <a class="nav-link" href="{{ url('recruiter/logout') }}">Logout</a>
                </span>
            </div>
        </nav>
        <div class="col applications">
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Applied By</th>
                    <th scope="col">Applied On</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($applications as $application)
                    <tr>
                        <td>{{ $application->title }}</td>
                        <td>{{ $application->email }}</td>
                        <td>{{ $application->created_at }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
        </div>
    </div>
</body>
</html>
