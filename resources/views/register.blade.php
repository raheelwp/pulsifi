<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pulsifi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}" />
</head>
<body>
    <div class="container-fluid h-100">
        <div class="row justify-content-center align-items-center h-100">
            <div class="col col-sm-6 col-md-6 col-lg-4 col-xl-3 jumbotron">
                <h4>Create an account</h4>
                <form action="{{ url('signup') }}" method="post">
                    {{ csrf_field() }} 
                    <div class="form-group">
                        <input name="email" class="form-control form-control-lg" placeholder="User email" type="text">
                    </div>
                    @if($errors->first('email'))
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                    <div class="form-group">
                        <input name="password" class="form-control form-control-lg" placeholder="Password" type="password">
                    </div>
                    @if($errors->first('password'))
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                    <div class="form-group">
                        <input name="password_confirmation" class="form-control form-control-lg" placeholder="Confirm Password" type="password">
                    </div>
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label">I am a </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="account_type" id="account_type" value="recruiter">
                            <label class="form-check-label" for="account_type">Recruiter</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="account_type" id="account_type" value="jobseeker">
                            <label class="form-check-label" for="account_type">Jobseeker</label>
                        </div>
                    </div>
                    @if($errors->first('account_type'))
                        <span class="text-danger">{{ $errors->first('account_type') }}</span>
                    @endif
                    <div class="form-group">
                        <button class="btn btn-dark btn-lg btn-block">Sign up</button>
                    </div>
                </form>
                <p><a href="{{ url('/') }}">Already have an account?</a></p>
            </div>
        </div>
    </div>
</body>
</html>
