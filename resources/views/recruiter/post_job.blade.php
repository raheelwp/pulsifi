<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pulsifi</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}" />
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="{{ url('recruiter/dashboard') }}">Pulsifi</a>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('recruiter/job/post') }}">Post Job</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ ('recruiter/applications') }}">View Applications</a>
                    </li>
                </ul>
                <span class="navbar-text">
                    <a class="nav-link" href="{{ url('recruiter/logout') }}">Logout</a>
                </span>
            </div>
        </nav>
        <div class="col job-post-form jumbotron">
            <form action="{{ url('recruiter/job/post') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
                    <div class="col-sm-10">
                        <input name="title" type="text" class="form-control form-control-lg" id="title">
                        @if($errors->first('title'))
                            <span class="text-danger float-right">{{ $errors->first('title') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="location" class="col-sm-2 col-form-label col-form-label-lg">Location</label>
                    <div class="col-sm-10">
                        <input name="location" type="text" class="form-control form-control-lg" id="location">
                        @if($errors->first('location'))
                            <span class="text-danger float-right">{{ $errors->first('location') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-2 col-form-label col-form-label-lg">Description</label>
                    <div class="col-sm-10">
                        <textarea name="description" class="form-control" id="description" rows="3"></textarea>
                        @if($errors->first('description'))
                            <span class="text-danger float-right">{{ $errors->first('description') }}</span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="date" class="col-sm-2 col-form-label col-form-label-lg">Date</label>
                    <div class="col-sm-10">
                        <input name="date" type="date" class="form-control form-control-lg" id="date">
                        @if($errors->first('date'))
                            <span class="text-danger float-right">{{ $errors->first('date') }}</span>
                        @endif   
                    </div>
                </div>         
                <button class="btn btn-dark float-right">Post Job</button>
            </form>
        </div>
    </div>
</body>
</html>
