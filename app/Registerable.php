<?php

namespace App;

interface Registerable 
{
    /**
     * @param string $email
     * @param string $password
     */
    public function store(string $email, string $password);
}
