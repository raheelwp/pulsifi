<?php

namespace App;

use App\User;

class Jobseeker extends User 
{
    /**
     * @var string
     */
    public $redirectTo = 'jobseeker/dashboard';

    /**
     * @var string
     */
    protected $table = 'jobseekers';
}
