<?php

namespace App\Http\Controllers;

use App\Registerable;
use App\Recruiter;
use App\Jobseeker;

trait RegisterableTrait
{
    /**
     * @param string $accountType
     * @return Registerable
     */
    private function getRegisterableEntity(string $accountType): Registerable
    {   
        return ($accountType === 'recruiter') ? new Recruiter : new Jobseeker;
    }
}
