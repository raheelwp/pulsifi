# Pulsifi

This is a test scenario for pulsifi job application. It is a simple job portal having following features.

  - Recruiter can create account
  - Jobseeker can create account
  - Recruiter can see his posted jobs
  - Jobseeker can apply to jobs
  - Recruiter can see job applications on his posted jobs
  - Jobseeker can see his applied jobs
  
## Installation

### Requirements
- git
- docker
- docker compose

### Installation
- clone this repository
- cd into the project folder
- run docker-compose -f docker/development.yml up --build -d
- open localhost:8080 in browser
