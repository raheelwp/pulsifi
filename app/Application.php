<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    /**
     * @var string
     */
    protected $table = 'applications';

    /**
     * @param Builder $query
     * @param int $id
     */
    public function scopeJobseeker(Builder $query, int $id): Builder
    {
        return $query->where('jobseeker_id', $id);
    }
}
